---
author: "Krishna Bhogaonker"
title: "Variational Approach to Climate Region Segmentation"
date: "2021-04-13"
categories:
    - "climate"
tags:
    - "segmentation"
draft: false
---

## Problem Description: Climate Segmentation 

This project is still very speculative and I am not sure it is doable. But, it
is always good to write something down and evaluate it. The

The term image segmentation represents a variety of approaches within the 
mathematical subfield of image processing. Often image segmentation refers to 
identifying different objects in an image. For example, and image might contain
a car or a person, and the mathematical task it to identify all of the pixels 
that refer to that car or person. In the machine learning world, this form 
of object segmentation is often called "semantic segmentation."

However, segmentation has been used in a more general context within image processing.
Segmentation may also refer so coarsening the resolution of an image to identify
lower frequency regions of similarity. In the image 
below, the original picture on the left captures all of the rich detail 
of the individual's face. There are detailed gradations in skin tone as 
well as patches reflecting more or less light. If we look toward the eye itself, 
there is a mixture of light and dark packages that compose the eye.

![Image Segmentation](../figures/variational_approach_climate_segmentation/image_segmentation.png)

As we coarsen the resolution of the image--in other words--remove the detail, 
we can see that common regions of dark and light begin to stand out. The center
image above uses a piece-wise approximation to the Mumford-Shah functional to 
gradually blur the differences between regions. The final image on the right 
uses the Chan-Vese binary approximation to distinguision between two regions 
in the image, either dark or light.

Thus, segmentation has the virtue of removing detail from an image so that the
central regions of variation are easier to observe.

## Applications of Segmentation to Climate Analysis

Once central problem in climate analysis, as we have discussed, is understanding 
the differential impact of temperature, precipitation, and water prices have 
on farmers in California. Some regions continue to have rain, temperature, and 
water patterns consistent with the longer term trend, while other regions are
seeing substantial deviations from their historical trend. The data from the 
California Agricultural Statistics demonstrate this as the case below. This is
just one section of the 2016-2017 annual California Agricultural Statistics document.
For the month of June, the report indicates that temperatures were 4.2 degrees 
above average and that precipitation was significantly below average. 


{{< figure src="../figures/variational_approach_climate_segmentation/agricultural_statistics.png" caption="agricultural statistics" >}}

While this image is just an anecdotal snapshot of one month in California, the 
story connects with a larger story. There are genuine shifts in the California 
growing season in different parts of the state. Some areas are experiencing higher
levels of precipitation and some are experiencing lower levels of precipitation. 
Other areas are experiencing higher temperatures while other regions are 
experiencing lower temperatures. Hence we would like to extract the boundaries
of these regions of variation to better forecast their demands for water, as 
well as estimating their expected farm output.  

The most common approach to a problem like this might be statistical. We might 
capture the agricultural statistics on a country by county basis and then 
cluster those counties by average rainfall or temperature. This approach is 
fast and simple, but it might not give us the accuracy that we would like. 
Just as in the image of the face above, the coarsening of resolution might 
provide more insight into the boundaries between impacted regions that statistical 
methods might not provide. Further, statistical methods handle the spatial 
adjacency of regions in a very coarse manner, which can impact the accuracy of 
predictions. 

## Variational Methods for Segmentation





























