---
author: "Krishna Bhogaonker"
title: "What Is Streamflow"
date: 2021-04-02T16:36:24-04:00
draft: false
---

According to the United States Geological Survey, the first stream gauges were
installed in 1889 along the Rio Grande River in New Mexico, to help farmers 
and the government determine whether there was enough water for agricultural
irrigation. The original intent of streamflow monitoring was to determine 
whether a water source could sustain the twin needs for an adequate supply of 
water and the predictability of that supply. 

